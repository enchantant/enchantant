# -*- coding: utf-8 -*-
# Last modified: Fri Apr  9 17:57:14 2010

"""

@author: Étienne BERSAC <etienne.bersac@corp.nerim.net>
"""

from nose.tools import set_trace
import os.path

def carrotage():
    from odf.opendocument import load
    from odf.office import Text
    from odf.text import P, Section, SectionSource, \
         TableOfContent, TableOfContentSource, IndexTitleTemplate, \
         AlphabeticalIndex, AlphabeticalIndexSource
    from odf.style import Style, \
         ParagraphProperties, TextProperties, PageLayoutProperties,\
         SectionProperties, Columns, Column

    # Charger le modèle
    modele = os.path.join(os.path.dirname(__file__), 'modele.odm')
    carnet = load(modele)

    children = list(carnet.text.childNodes)
    for child in children:
        carnet.text.removeChild(child)

    s = carnet.automaticstyles

    stoc = Style(name='TableOfContent', family='section')
    s.addElement(stoc)
    stocp = SectionProperties()
    stoc.addElement(stocp)
    stocp.addElement(Columns(columncount=3, columngap='0.2cm'))


    # Ajout des chants
    categories = [(u'prieres.odt', ['chant1.odt', 'chant2.odt', 'chant3.odt']),
                  (u'veillees.odt', ['chant4.odt'])]
    for i, (fichier, chants) in enumerate(categories):
        nom = 'Categorie%d'%(i+1)
        section = Section(name=nom)
        carnet.text.addElement(section)
        href = os.path.join('..', 'carrotage', fichier)
        section.addElement(SectionSource(href=href))

        for j, fichier in enumerate(chants):
            nom = 'Chant%d'%(j+1)
            section = Section(name=nom)
            href = os.path.join('..', 'carrotage', fichier)
            section.addElement(SectionSource(href=href))
            carnet.text.addElement(section)

    # Table des matières
    toc = TableOfContent(name="TableDesMatieres", stylename=stoc)
    carnet.text.addElement(toc)
    tocs = TableOfContentSource(outlinelevel=5)
    toc.addElement(tocs)
    toch = IndexTitleTemplate()
    tocs.addElement(toch)
    toch.addText(u'Index par catégories')

    # Index alphabêtique
    ai = AlphabeticalIndex(name="IndexAlphabetique", stylename=stoc)
    carnet.text.addElement(ai)
    ais = AlphabeticalIndexSource(alphabeticalseparators='true')
    ai.addElement(ais)
    aih = IndexTitleTemplate()
    ais.addElement(aih)
    aih.addText(u'Index alphabêtique')

    carnet.save('carnet.odm')
