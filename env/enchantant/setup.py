# -*- coding: iso-8859-15 -*-
# Last modified: Fri Apr  9 16:49:56 2010

"""

@author: �tienne BERSAC <etienne.bersac@corp.nerim.net>
"""

try:
    from setuptools import setup, find_packages
except ImportError:
    from ez_setup import use_setuptools
    use_setuptools()
    from setuptools import setup, find_packages

setup(
    name='chahut',
    version='0.0.1',
    install_requires=['odfpy'],
    packages=find_packages(exclude=['ez_setup']),
    include_package_data=True,
    zip_safe=False,
)
